//
//  MessageViewController.m
//  I-Show
//
//  Created by 林辉武 on 14/12/28.
//  Copyright (c) 2014年 ___mx_office___. All rights reserved.
//

#import "MessageViewController.h"
#import "CustomRefreshTableView.h"

#define kNewBtnTag 2001
#define kInviteBtnTag 2002
#define kRefreshTableTag 2003

@interface MessageViewController ()<CustomRefreshTableViewDataSource,CustomRefreshTableViewDelegate>
{
    UIButton *_newBtn;
    UIButton *_inviteBtn;
    
    CustomRefreshTableView *_refreshTableView;
    
}

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初始化titleview
    [self initTitleView];
    
    _refreshTableView = [[CustomRefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64) style:UITableViewStylePlain];
    _refreshTableView.delegate = self;
    _refreshTableView.dataSource = self;
    _refreshTableView.tag = kRefreshTableTag;
    [self.view addSubview:_refreshTableView];
    
}

-(void)initTitleView
{
    _newBtn = [MAUiFactory createButtonWithRect:CGRectMake(0, 0, 44, 26) title:@"最新" titleFont:default_font12 titleColor:[UIColor whiteColor] normal:@"btn_message_left_nor.png" highlight:nil selected:@"btn_message_left_sel.png" selector:@selector(titleBtnHandler:) target:self];
    _newBtn.tag = kNewBtnTag;
    
    UIImageView *lineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_message.png"]];
    lineView.frame = CGRectMake(_newBtn.x+_newBtn.width, 0, 1, 26);
    
    _inviteBtn = [MAUiFactory createButtonWithRect:CGRectMake(lineView.x+lineView.width, 0, 44, 26) title:@"邀请" titleFont:default_font12 titleColor:[UIColor whiteColor] normal:@"btn_message_right_nor.png" highlight:nil selected:@"btn_message_right_sel.png" selector:@selector(titleBtnHandler:) target:self];
    _inviteBtn.tag = kInviteBtnTag;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 26)];
    titleView.backgroundColor = [UIColor clearColor];
    [titleView addSubview:_newBtn];
    [titleView addSubview:lineView];
    [titleView addSubview:_inviteBtn];
    
    self.navigationItem.titleView = titleView;
    
    [self titleBtnHandler:_newBtn];
}

-(void)titleBtnHandler:(UIButton *)button
{
    switch (button.tag) {
        case kNewBtnTag:
        {
            button.selected = YES;
            _inviteBtn.selected = NO;
        }
            break;
        case kInviteBtnTag:
        {
            button.selected = YES;
            _newBtn.selected = NO;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - CustomRefreshTableViewDelegate

- (CGFloat)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (NSInteger)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView numberOfRowsInSection:(NSInteger)section
{
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            return 5;
        }
            break;
        default:
            break;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInCustomRefreshTableView:(CustomRefreshTableView *)customRefreshTableView
{
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            return 1;
        }
            break;
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [customRefreshTableView.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [customRefreshTableView.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            
        }
            break;
        default:
            break;
    }
}

//-(NSArray *)sectionIndexTitlesForCustomRefreshTableView:(CustomRefreshTableView *)customRefreshTableView
//{
//    switch (customRefreshTableView.tag) {
//            
//        case 1001:  //我的客户
//        {
//            return _allCustomFirstLetterArray;
//        }
//            break;
//        case 1002:  //下属客户
//        {
//            return _branchCustomFirstLetterArray;
//        }
//            break;
//        case 1003:  //附近客户
//        {
//            return _neiborCustomFirstLetterArray;
//        }
//            break;
//            
//        default:
//            break;
//    }
//    
//    return nil;
//}

//- (CGFloat)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView heightForHeaderInSection:(NSInteger)section
//{
//    return 20;
//}

//- (UIView *)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView viewForHeaderInSection:(NSInteger)section
//{
//    SectionTitleView *sectionTitleView = nil;
//    
//    switch (customRefreshTableView.tag) {
//            
//        case 1001:  //我的客户
//        {
//            if (_allCustomFirstLetterArray && _allCustomFirstLetterArray.count>0) {
//                
//                sectionTitleView = [[SectionTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
//                
//                sectionTitleView.titleLab.text = _allCustomFirstLetterArray[section];
//            }
//            
//        }
//            break;
//        case 1002:  //下属客户
//        {
//            if (_branchCustomFirstLetterArray && _branchCustomFirstLetterArray.count>0) {
//                
//                sectionTitleView = [[SectionTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
//                
//                sectionTitleView.titleLab.text = _branchCustomFirstLetterArray[section];
//            }
//        }
//            break;
//        case 1003:  //附近客户
//        {
//            if (_neiborCustomFirstLetterArray && _neiborCustomFirstLetterArray.count>0) {
//                
//                sectionTitleView = [[SectionTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
//                
//                sectionTitleView.titleLab.text = _neiborCustomFirstLetterArray[section];
//            }
//        }
//            break;
//            
//        default:
//            break;
//    }
//    
//    return sectionTitleView;
//}

-(void)customRefreshTableViewDidRefreshHeaderView:(CustomRefreshTableView *)customRefreshTableView
{
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            
        }
            break;
        default:
            break;
    }
}

-(void)customRefreshTableViewDidRefreshFooterView:(CustomRefreshTableView *)customRefreshTableView
{
    switch (customRefreshTableView.tag) {
        case kRefreshTableTag:
        {
            
        }
            break;
        default:
            break;
    }
    
}

-(void)customRefreshTableViewDidfinishedLoadData:(CustomRefreshTableView *)customRefreshTableView
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
