//
//  LoginViewController.m
//  I-Show
//
//  Created by 林辉武 on 14/12/27.
//  Copyright (c) 2014年 ___mx_office___. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"登录";
    
    UIButton *leftBtn = [MAUiFactory createButtonWithRect:CGRectMake(0, 0, 44, 44) title:@"取消" titleFont:nil titleColor:[UIColor whiteColor] normal:nil highlight:nil selected:nil selector:@selector(leftHandler) target:self];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [MAUiFactory createButtonWithRect:CGRectMake(0, 0, 44, 44) title:@"注册" titleFont:nil titleColor:[UIColor whiteColor] normal:nil highlight:nil selected:nil selector:@selector(rightHandler) target:self];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)leftHandler
{
    [self popViewController];
}

-(void)rightHandler
{
    UIViewController *viewCtrl = [[UIViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:viewCtrl animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login
{
    
}

-(IBAction)forgetPassword
{
    
}





@end
