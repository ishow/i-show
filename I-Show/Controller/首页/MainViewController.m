//
//  MainViewController.m
//  I-Show
//
//  Created by 林辉武 on 14/12/28.
//  Copyright (c) 2014年 ___mx_office___. All rights reserved.
//

#import "MainViewController.h"
#import "MessageViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    UIButton *rightBtn = [MAUiFactory createButtonWithRect:CGRectMake(0, 0, 44, 44) title:@"消息" titleFont:nil titleColor:[UIColor whiteColor] normal:nil highlight:nil selected:nil selector:@selector(rightHandler) target:self];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)rightHandler
{
    MessageViewController *viewCtrl = [[MessageViewController alloc] init];
    viewCtrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewCtrl animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
