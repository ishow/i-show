//
//  ToolUtils.m
//  拍卖行
//
//  Created by admin on 13-8-29.
//  Copyright (c) 2013年 liouly. All rights reserved.
//

#import "ToolUtils.h"
#import "ChineseString.h"
#import "pinyin.h"
#import <CommonCrypto/CommonDigest.h>

@implementation ToolUtils

#pragma  mark - 文件路径

//返回Cache路径
+(NSString *)returnCachePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask,YES);
    
    NSString *cachesDir = [paths objectAtIndex:0];
    
    return cachesDir;
}


//返回首字母数组
+(NSMutableArray *)chineseSourtOutToFirstLetter:(NSMutableArray *)array
{
    
    //Step1:初始化
    NSMutableArray *stringsToSort=array;
    
    //Step2:获取字符串中文字的拼音首字母并与字符串共同存放
    NSMutableArray *chineseStringsArray=[NSMutableArray array];
    for(int i=0;i<[stringsToSort count];i++){
        ChineseString *chineseString=[[ChineseString alloc]init];
        
        chineseString.string=[NSString stringWithString:[stringsToSort objectAtIndex:i]];
        
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            NSString *pinYinResult=[NSString string];
            for(int j=0;j<chineseString.string.length;j++){
                NSString *singlePinyinLetter=[[NSString stringWithFormat:@"%c",pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult=[pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin=pinYinResult;
            chineseString.firstLetter = [chineseString.pinYin substringToIndex:1];
        }else{
            chineseString.pinYin=@"";
            chineseString.firstLetter = @"";
        }
        [chineseStringsArray addObject:chineseString];
    }
    
    // Step4:把内容从ChineseString类中提取出来
    NSMutableArray *firstLetterArray = [NSMutableArray array];
    for(int i=0;i<[chineseStringsArray count];i++){
        [firstLetterArray addObject:((ChineseString*)[chineseStringsArray objectAtIndex:i]).firstLetter];
    }
    
    //返回首字母数组
    return firstLetterArray;
}

//按照key对数组进行排序
+(NSMutableArray *)sortWithDescriptor:(NSMutableArray *)arrayToSort Key:(NSString *)key
{
    NSMutableArray *chineseStringsArray = [NSMutableArray arrayWithArray:arrayToSort];
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:key ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    return chineseStringsArray;
}

//获得Label的高度
+(CGFloat)heightForLabel:(UILabel *)contentLab
{
    
    CGSize size = [contentLab.text sizeWithFont:contentLab.font
                              constrainedToSize: CGSizeMake(contentLab.frame.size.width, CGFLOAT_MAX)
                                  lineBreakMode: NSLineBreakByWordWrapping];
    
    return size.height;
    
}

//获得Label的宽度
+(CGFloat)widthForLabel:(UILabel *)contentLab
{
    CGSize size = [contentLab.text sizeWithFont:contentLab.font
                              constrainedToSize:CGSizeMake(CGFLOAT_MAX, contentLab.frame.size.height)
                                  lineBreakMode:NSLineBreakByWordWrapping];
    
    return size.width;
}

//获得TextView的高度
+(CGFloat)heightForTextView:(UITextView *)textView
{
    CGSize size = [textView.text sizeWithFont:textView.font
                              constrainedToSize:CGSizeMake(textView.frame.size.width,CGFLOAT_MAX)
                                  lineBreakMode:NSLineBreakByWordWrapping];
    
    return size.width;
}

//保存数据到本地沙盒
+(void)setDataToSandboxWithPersonalInfo:(NSDictionary *)dicToSandbox
{
    NSMutableDictionary *data;
    
    //获得沙盒路径
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *fileName = [path stringByAppendingPathComponent:@"personal.plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:fileName];
    
    if (!dic) {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm createFileAtPath:fileName contents:nil attributes:nil];
        data = [NSMutableDictionary dictionaryWithDictionary:dicToSandbox];
        [data writeToFile:fileName atomically:YES];

    }
}

//读取沙盒中的数据
+(NSDictionary *)getDataFromSanboxWithPersonalInfo
{
    //获得沙盒路径
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *fileName = [path stringByAppendingPathComponent:@"personal.plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:fileName];
    
    if (dic) {
        
        return dic;
        
    }else{
        
        return nil;
        
    }
}

//删除沙盒中的数据
+(void)deleteDataInSanbox
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *fileName = [path stringByAppendingPathComponent:@"personal.plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:fileName];
    
    if (dic) {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:fileName error:nil];

    }

}

//判断沙盒中是否有数据
+(BOOL)isDataInSanbox
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *fileName = [path stringByAppendingPathComponent:@"personal.plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:fileName];
    
    if (dic) {
        
        return YES;
    }
    
    return NO;
}


#pragma mark - 本地缓存文件操作

//计算指定文件夹下的文件总大小
+(float )folderSizeAtPath:(NSString*) folderPath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    float folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += (float)[self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/1024;
}

//删除指定文件夹下的文件
+(void)removeFolderWithPath:(NSString *)folderPath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:folderPath]){
        
        NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
        NSString* fileName;

        while ((fileName = [childFilesEnumerator nextObject]) != nil){
            NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
            [manager removeItemAtPath:fileAbsolutePath error:nil];
        }
    }
}

//计算指定文件的大小
+(float)fileSizeAtPath:(NSString*) filePath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return (float)[[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

//删除本地的指定文件
+(void)deleteFileWithFileName:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    NSString *file = [path stringByAppendingPathComponent:fileName];
    
    if (fileName) {
        
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:file error:nil];
        
    }
}
+ (void)showHUD:(NSString *)text andView:(UIView *)view andHUD:(MBProgressHUD *)hud
{
    [view addSubview:hud];
    hud.labelText = text;
    //    hud.dimBackground = YES;
    hud.square = YES;
    [hud show:YES];
}

+ (void)showCustomViewHUD:(NSString *)text imageName:(NSString *)image andViewController:(UIViewController*)cv
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:cv.view];
	[cv.view addSubview:HUD];
	
	// The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
	// Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
	HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:image]];
	
	// Set custom view mode
	HUD.mode = MBProgressHUDModeCustomView;
	
    //	HUD.delegate = cv;
	HUD.labelText = text;
	
	[HUD show:YES];
	[HUD hide:YES afterDelay:1];
}

+ (void)showCustomViewHUD:(NSString *)text imageName:(NSString *)image andView:(UIView *)view
{
    MBProgressHUD *HUD =[[MBProgressHUD alloc] initWithView:view];
	[view addSubview:HUD];
	
	// The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
	// Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
	HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:image]];
	
	// Set custom view mode
	HUD.mode = MBProgressHUDModeCustomView;
	
    //	HUD.delegate = cv;
	HUD.labelText = text;
	
	[HUD show:YES];
	[HUD hide:YES afterDelay:1];
}
//对字符串进行md5加密
+ (NSString *)md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
+ (BOOL)checkEmail:(NSString *)str
{
    NSString * regex = @"^([a-z0-9A-Z]+[-|\\._]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}
+(BOOL)checkPhone:(NSString*)checkString{
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    NSString * FX=@"^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$";
    NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    NSPredicate *regextestfx = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", FX];
    NSPredicate *regextestphs = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    if (([regextestmobile evaluateWithObject:checkString] == YES)
        || ([regextestcm evaluateWithObject:checkString] == YES)
        || ([regextestct evaluateWithObject:checkString] == YES)
        || ([regextestcu evaluateWithObject:checkString] == YES)
        || ([regextestfx evaluateWithObject:checkString] == YES)
        || ([regextestphs evaluateWithObject:checkString] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (UIColor *) colorWithHexString: (NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //r
    NSString *rString = [cString substringWithRange:range];
    
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

+ (UIColor *) colorWithHexString: (NSString *)color andAlpha:(CGFloat )alpha
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //r
    NSString *rString = [cString substringWithRange:range];
    
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:alpha];
}

//返回完整的图片路径
+(NSString *)orgianPath:(NSString *)imagePath
{
//    NSString *path = [NSString stringWithFormat:@"%@%@",BASEIMAGEURL,imagePath];
    
    return imagePath;
}

@end
