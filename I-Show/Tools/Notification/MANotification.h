//
//  MANotification.h
//  公用类
//
//  Created by 哈 哈 on 14-10-27.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MANotification : NSObject
+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction;

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName;


+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName fireDate:(NSDate *)fireDate;

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction fireDate:(NSDate *)fireDate;

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName fireDate:(NSDate *)fireDate userInfo:(NSDictionary *)userInfo;


-(void)cancelAllLocalNotifications;
-(void)cancelLocalNotification:(UILocalNotification *)notification;
@end
