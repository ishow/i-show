//
//  MANotification.m
//  公用类
//
//  Created by 哈 哈 on 14-10-27.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MANotification.h"

@implementation MANotification


#pragma mark 创建
+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction{
    return [MANotification createLocalNontification:alertBody alertAction:alertAction soundName:nil fireDate:nil userInfo:nil];
}

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName{
    return [MANotification createLocalNontification:alertBody alertAction:alertAction soundName:soundName fireDate:nil userInfo:nil];
}

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName fireDate:(NSDate *)fireDate{
    return [MANotification createLocalNontification:alertBody alertAction:alertAction soundName:soundName fireDate:fireDate userInfo:nil];
}

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction fireDate:(NSDate *)fireDate{
    return [MANotification createLocalNontification:alertBody alertAction:alertAction soundName:nil fireDate:fireDate userInfo:nil];
}

+(UILocalNotification *)createLocalNontification:(NSString *)alertBody alertAction:(NSString *)alertAction soundName:(NSString *)soundName fireDate:(NSDate *)fireDate userInfo:(NSDictionary *)userInfo{
    
    //iOS8系统上需要注册本地通知，这样才能正常使用
    // iOS8注册本地通知
    if (([[[UIDevice currentDevice] systemVersion] compare:@"8" options:NSNumericSearch] != NSOrderedAscending)) {
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
            
        {
            
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
            
        }
    }
    
    
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    if (notification!=nil) {
        //指定时间通知
        if (fireDate) {
            notification.fireDate = fireDate;
        }
        //使用本地时区
        notification.timeZone=[NSTimeZone defaultTimeZone];
        notification.alertBody=alertBody;
        notification.alertAction = alertAction;
//        //重复的间隔
//        notification.repeatCalendar = [NSCalendar currentCalendar];
        //重复的次数
//        notification.repeatInterval = 1;
        if (soundName.length>0) {
            notification.soundName = soundName;
        }else{
            //通知提示音 使用默认的
            notification.soundName= UILocalNotificationDefaultSoundName;
        }
        //这个通知到时间时，你的应用程序右上角显示的数字。
//        notification.applicationIconBadgeNumber = 1;
        
        //add key  给这个通知增加key 便于半路取消。nfkey这个key是我自己随便起的。
        // 假如你的通知不会在还没到时间的时候手动取消 那下面的两行代码你可以不用写了。
        if (userInfo) {
            [notification setUserInfo:userInfo];
        }
        //启动这个通知
        [[UIApplication sharedApplication]   scheduleLocalNotification:notification];
        //这句真的特别特别重要。如果不加这一句，通知到时间了，发现顶部通知栏提示的地方有了，然后你通过通知栏进去，然后你发现通知栏里边还有这个提示
        //除非你手动清除，这当然不是我们希望的。加上这一句就好了。网上很多代码都没有，就比较郁闷了。
    }
    return notification;
}

#pragma mark 取消所有的
-(void)cancelAllLocalNotifications{
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
}

#pragma mark 取消指定的
-(void)cancelLocalNotification:(UILocalNotification *)notification {
    [[UIApplication sharedApplication]cancelLocalNotification:notification];
}
@end
