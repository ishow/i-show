//
//  ToolUtils.h
//  拍卖行
//
//  Created by admin on 13-8-29.
//  Copyright (c) 2013年 liouly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface ToolUtils : NSObject

//返回Cache路径
+(NSString *)returnCachePath;

//返回首字母数组
+(NSMutableArray *)chineseSourtOutToFirstLetter:(NSMutableArray *)array;

//按照key对数组进行排序
+(NSMutableArray *)sortWithDescriptor:(NSMutableArray *)arrayToSort Key:(NSString *)key;

//获得Label的高度
+(CGFloat)heightForLabel:(UILabel *)contentLab;

//获得Label的宽度
+(CGFloat)widthForLabel:(UILabel *)contentLab;

//获得TextView的高度
+(CGFloat)heightForTextView:(UITextView *)textView;

//保存数据到本地沙盒
+(void)setDataToSandboxWithPersonalInfo:(NSDictionary *)dicToSandbox;

//读取沙盒中的数据
+(NSDictionary *)getDataFromSanboxWithPersonalInfo;

//删除沙盒中的数据
+(void)deleteDataInSanbox;

//判断沙盒中是否有数据
+(BOOL)isDataInSanbox;

//计算指定文件夹下的文件总大小
+(float)folderSizeAtPath:(NSString*) folderPath;

//计算指定文件的大小
+(float)fileSizeAtPath:(NSString*) filePath;

//删除指定文件夹下的文件
+(void)removeFolderWithPath:(NSString *)folderPath;

//删除本地的指定文件
+(void)deleteFileWithFileName:(NSString *)fileName;

//检测邮箱是否正确
+ (BOOL)checkEmail:(NSString *)str;

//加载提示框
+ (void)showHUD:(NSString *)text andView:(UIView *)view andHUD:(MBProgressHUD *)hud;
//提示框
+ (void)showCustomViewHUD:(NSString *)text imageName:(NSString *)image andViewController:(UIViewController *)view;
//
//提示框
+ (void)showCustomViewHUD:(NSString *)text imageName:(NSString *)image andView:(UIView *)view;
+ (NSString *)md5:(NSString *)str;
+(BOOL)checkPhone:(NSString*)checkString;

//16进制颜色获取
+ (UIColor *) colorWithHexString: (NSString *)color;
+ (UIColor *) colorWithHexString: (NSString *)color andAlpha:(CGFloat )alpha;

//返回完整的图片路径
+(NSString *)orgianPath:(NSString *)imagePath;

@end
