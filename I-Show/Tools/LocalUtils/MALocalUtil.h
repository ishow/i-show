//
//  LocalUtil.h
//  公用类
//  本地存储的工具类,包括相片存储，沙箱存储，对你存储，keyChain存储(卸载应用不会被删除)
//  Created by 哈 哈 on 14-9-15.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MALocalUtil : NSObject

+(NSString *)documentPath;

+(NSString *)resourcePathWithFileName:(NSString *)name andType:(NSString *)type;

//保存到NSUserDefaults
+(void)saveWithKey:(NSString *)key andValue:(id)value;

//读取NSUserDefaults中的内容
+(id)readWithKey:(NSString *)key;

//根据指定路径保存数据
+(BOOL)saveDataWithPath:(NSString *)path andData:(NSData *)data;

//根据路径读取文件
+(NSData *)readDataWithPath:(NSString *)path error:(NSError **)errorPtr;

//保存图片到相册
+(void)savePhotoToLibrary:(UIImage *)image andSel:(SEL)sel andTarget:(id)target;

//保存图片到指定路径
+(BOOL)savePhotoWithPath:(NSString *)path andImage:(UIImage *)image;

//保存对象到指定路径
+(BOOL)saveArchiverWithObj:(id)rootObject andPath:(NSString *)path;

//根据指定路径读取
+(id)readArchiverWithPath:(NSString *)path;

//保存到keyChain
+ (void)saveKeyChain:(NSString *)key value:(id)data;
//读取keyChain
+ (id)readKeyChain:(NSString *)key;
//删除keyChain
+ (void)deleteKeyChain:(NSString *)key;
@end
