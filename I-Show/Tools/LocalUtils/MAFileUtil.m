//
//  FileUtil.m
//  HuoQiuJiZhang
//
//  Created by 喻平 on 13-4-24.
//  Copyright (c) 2013年 com.huoqiu. All rights reserved.
//

#import "MAFileUtil.h"
#import <dirent.h>
#import <sys/stat.h>
#import "NSString+Extend.h"

@implementation MAFileUtil

#pragma mark 创建文件夹
+ (BOOL)createDirectoryAtPath:(NSString *)path
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return YES;
    }
    return [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
}

#pragma mark在指定目录创建文件
+ (BOOL)createFileAtDirectory:(NSString *)path fileName:(NSString *)fileName
{
    
    return [MAFileUtil createFileAtPath:[path stringByAppendingFormat:@"/%@", fileName]];
}

#pragma mark 创建文件
+ (BOOL)createFileAtPath:(NSString *)path
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return YES;
    }
    return [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
}


#pragma mark 在文档目录创建文件夹
+ (BOOL)createDirectoryAtDocument:(NSString *)path
{
    return [self createDirectoryAtPath:[[MAFileUtil applicationDocumentsDirectory] stringByAppendingString:path]];
}


#pragma mark 返回文档路径
+ (NSString *)directory:(int) type
{
    return [NSSearchPathForDirectoriesInDomains(type, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)applicationDocumentsDirectory
{
	return [self directory:NSDocumentDirectory];
}

+ (NSString *)applicationStorageDirectory
{
    NSString *applicationName = [[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString *)kCFBundleNameKey];
    return [[self directory:NSApplicationSupportDirectory] stringByAppendingPathComponent:applicationName];
}

#pragma mark 向指定文件添加字符串，如果文件不存在则创建
+ (BOOL)appendStringToFile:(NSString *)path string:(NSString *)string
{
    if ([string isEmpty]) {
        return YES;
    }
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (!exists) {
        if (![MAFileUtil createFileAtPath:path]) {
            return NO;
        }
    }
    // 写入文件
    NSFileHandle *file = [NSFileHandle fileHandleForWritingAtPath:path];
    [file seekToEndOfFile];
    [file writeData:[string dataUsingEncoding:NSUTF8StringEncoding]];
    [file closeFile];
    return YES;
}

+ (NSString *)readStringFromFile:(NSString *)path
{
    if (!path || ![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

+ (BOOL)removeFile:(NSURL *)url
{
    return [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
}

+ (NSString *)getFolderSize:(long long)size {
    if (size < 1000) {
        return [NSString stringWithFormat:@"%lldB", size];
    }
    else if (size < 1000 * 1000) {
        return [NSString stringWithFormat:@"%.2fKB", size * 1.0 / 1000];
    }
    else if (size < 1000 * 1000 * 1000) {
        return [NSString stringWithFormat:@"%.2fMB", size * 1.0 / 1000 / 1000];
    }
    else if (size < 1000.0 * 1000.0 * 1000.0 * 1000.0) {
        return [NSString stringWithFormat:@"%.2fGB", size * 1.0 / 1000 / 1000 / 1000];
    }
    return @"";
}

+ (long long)folderSizeAtPath:(NSString *)folderPath {
    return [self _folderSizeAtPath:[folderPath cStringUsingEncoding:NSUTF8StringEncoding]];
}

+ (long long)_folderSizeAtPath:(const char *)folderPath {
    long long folderSize = 0;
    DIR *dir = opendir(folderPath);
    if (dir == NULL) return 0;
    struct dirent *child;
    while ((child = readdir(dir)) != NULL) {
        if (child->d_type == DT_DIR && (
                                        (child->d_name[0] == '.' && child->d_name[1] == 0) ||                         // 忽略目录 .
                                        (child->d_name[0] == '.' && child->d_name[1] == '.' && child->d_name[2] == 0)                         // 忽略目录 ..
                                        )) continue;
        
        NSUInteger folderPathLength = strlen(folderPath);
        char childPath[1024]; // 子文件的路径地址
        stpcpy(childPath, folderPath);
        if (folderPath[folderPathLength - 1] != '/') {
            childPath[folderPathLength] = '/';
            folderPathLength++;
        }
        stpcpy(childPath + folderPathLength, child->d_name);
        childPath[folderPathLength + child->d_namlen] = 0;
        if (child->d_type == DT_DIR) { // directory
            folderSize += [self _folderSizeAtPath:childPath]; // 递归调用子目录
            // 把目录本身所占的空间也加上
            struct stat st;
            if (lstat(childPath, &st) == 0) folderSize += st.st_size;
        }
        else if (child->d_type == DT_REG || child->d_type == DT_LNK) { // file or link
            struct stat st;
            if (lstat(childPath, &st) == 0) folderSize += st.st_size;
        }
    }
    return folderSize;
}

+ (void)deleteAll:(NSString *)cachePath {
    [[NSFileManager defaultManager] removeItemAtPath:cachePath error:NULL];
    [[NSFileManager defaultManager] createDirectoryAtPath:cachePath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:NULL];
}
@end
