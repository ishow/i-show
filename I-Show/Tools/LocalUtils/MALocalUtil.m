//
//  LocalUtil.m
//  公用类
//
//  Created by 哈 哈 on 14-9-15.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MALocalUtil.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIAlertView+Extend.h"

@implementation MALocalUtil

#pragma mark 获取document的路径
+(NSString *)documentPath{
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    return documents;
}

#pragma mark 根据文件名和文件类型获取项目中的路径
+(NSString *)resourcePathWithFileName:(NSString *)name andType:(NSString *)type{
    NSString *path = [[NSBundle mainBundle]pathForResource:name ofType:type];
    return path;
}
#pragma mark 保存到NSUserDefaults
+(void)saveWithKey:(NSString *)key andValue:(id)value{
    NSUserDefaults *load = [NSUserDefaults standardUserDefaults];
	[load setObject:value forKey:key];
	[load synchronize];
}

#pragma mark 读取NSUserDefaults中的内容
+(id)readWithKey:(NSString *)key{
    NSUserDefaults *load = [NSUserDefaults standardUserDefaults];
	return [load objectForKey:key];
}

#pragma mark 根据指定路径保存数据
+(BOOL)saveDataWithPath:(NSString *)path andData:(NSData *)data{
    BOOL result = NO;
    result = [data writeToFile:path atomically:YES];
    return result;
}

#pragma mark 根据路径读取文件
+(NSData *)readDataWithPath:(NSString *)path error:(NSError **)errorPtr{
    NSData *data = [NSData dataWithContentsOfFile:path options:0 error:errorPtr];
    return data;
}

#pragma mark 保存图片到相册
+(void)savePhotoToLibrary:(UIImage *)image andSel:(SEL)sel andTarget:(id)target{
    if ([MALocalUtil checkPhotoLibraryLimit]) {
        UIImageWriteToSavedPhotosAlbum(image, target, sel, nil);
    }
}

#pragma mark 判断是否有保存相片的权限
+(BOOL)checkPhotoLibraryLimit{
    if ( [ALAssetsLibrary authorizationStatus] !=  ALAuthorizationStatusDenied ){
        return YES;
    }else{
        NSString *app_name = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
        [UIAlertView showAlertWithTitle:nil message:[NSString stringWithFormat:@"请打开系统设置中‘隐私—照片’，允许‘%@’使用您的照片。",app_name] cancelButtonTitle:@"确定" otherButtonTitle:nil onDisinss:^(int buttonIndex) {
            
        }];
        return NO;
    }
}

#pragma mark 保存图片到指定路径
+(BOOL)savePhotoWithPath:(NSString *)path andImage:(UIImage *)image{
    BOOL result = NO;
    result = [UIImagePNGRepresentation(image)writeToFile: path    atomically:YES];
    return result;
}

#pragma mark 保存对象到指定路径
+(BOOL)saveArchiverWithObj:(id)rootObject andPath:(NSString *)path{
    return [NSKeyedArchiver archiveRootObject:rootObject toFile:path];
}

#pragma mark 根据指定路径读取
+(id)readArchiverWithPath:(NSString *)path{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}



+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)kSecClassGenericPassword,(__bridge id)kSecClass,
            service, (__bridge id)kSecAttrService,
            service, (__bridge id)kSecAttrAccount,
            (__bridge id)kSecAttrAccessibleAfterFirstUnlock,(__bridge id)kSecAttrAccessible,
            nil];
}

#pragma mark 保存到keyChain
+ (void)saveKeyChain:(NSString *)key value:(id)data{
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge id)kSecValueData];
    SecItemAdd((__bridge CFDictionaryRef)keychainQuery, NULL);
}

#pragma mark 读取keyChain
+ (id)readKeyChain:(NSString *)key{
    id ret = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    [keychainQuery setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [keychainQuery setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((__bridge CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
        } @catch (NSException *e) {
            NSLog(@"Unarchive of %@ failed: %@", key, e);
        } @finally {
        }
    }
    if (keyData)
        CFRelease(keyData);
    return ret;
}
#pragma mark 删除keyChain
+ (void)deleteKeyChain:(NSString *)key{
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:key];
    SecItemDelete((__bridge CFDictionaryRef)keychainQuery);
}
@end
