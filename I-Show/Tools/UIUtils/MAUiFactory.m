//
//  MAUiFactory.m
//  公用类
//
//  Created by 哈 哈 on 14-10-14.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MAUiFactory.h"

@implementation MAUiFactory
+(MAUiFactory *)sharedInstance{
    static MAUiFactory *shareInstance;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        shareInstance = [[MAUiFactory alloc]init];
    });
    return shareInstance;
}

#pragma mark label
+ (void)setLable:(UILabel *)label
            font:(UIFont *)font
           title:(NSString *)title
{
    if (label) {
        label.font  = font;
        label.text  = title;
    }
}

+ (void)setLable:(UILabel *)label
         numberOfLine:(NSInteger)numberOfLine{
    if (label) {
        label.numberOfLines = numberOfLine;
        label.lineBreakMode = NSLineBreakByWordWrapping;
    }
}

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text{
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];//IOS7以下默认为白色，需要设置成透明
    label.text = text;
    return label;
}

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text
      backgroundColor:(UIColor *)backgroundColor{
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.backgroundColor = backgroundColor;
    return label;
}

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text
                 font:(UIFont *)font
            textColor:(UIColor *)textColor
      backgroundColor:(UIColor *)backgroundColor{
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text      = text;
    label.font      = font;
    label.textColor = textColor;
    label.backgroundColor = backgroundColor;
    return label;
}

#pragma mark uitextfield
+ (id)createTextFieldWith:(CGRect)frame
              placeholder:(NSString *)placeholder{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.placeholder = placeholder;
    // Default property
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.enablesReturnKeyAutomatically = YES;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    return textField;
}
+ (id)createTextFieldWith:(CGRect)frame
                 delegate:(id<UITextFieldDelegate>)delegate
            returnKeyType:(UIReturnKeyType)returnKeyType
          secureTextEntry:(BOOL)secureTextEntry
              placeholder:(NSString *)placeholder
                     font:(UIFont *)font {
    UITextField *textField = [MAUiFactory createTextFieldWith:frame placeholder:placeholder];
    textField.delegate          = delegate;
    textField.returnKeyType     = returnKeyType;
    textField.secureTextEntry   = secureTextEntry;
    textField.font              = font;
    return textField;
}

+ (UITextField *)createTextFieldWithRect:(CGRect)frame
                            keyboardType:(UIKeyboardType)keyboardType
                                  secure:(BOOL)secure
                             placeholder:(NSString *)placeholder
                                    font:(UIFont *)font
                                   color:(UIColor *)color
                                delegate:(id<UITextFieldDelegate>)delegate
{
    UITextField *textField = [MAUiFactory createTextFieldWith:frame placeholder:placeholder];
    textField.delegate          = delegate;
    textField.font              = font;
    textField.textColor         = color;
    textField.keyboardType      = keyboardType;
    textField.returnKeyType     = UIReturnKeyNext;
    textField.secureTextEntry   = secure;
    
    return textField;
}

#pragma mark uibutton
//Button(只有背景图)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                            normal:(NSString *)normalImage
                         highlight:(NSString *)clickIamge
                          selector:(SEL)selector
                            target:(id)target
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    
    if (normalImage != nil)
        [button setBackgroundImage:[UIImage imageNamed:normalImage]
                          forState:UIControlStateNormal];
    
    if (clickIamge != nil)
        [button setBackgroundImage:[UIImage imageNamed:clickIamge]
                          forState:UIControlStateHighlighted];
    
    if ((selector != nil) && (target != nil))
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


//Button(标题+背景图)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                             title:(NSString *)title
                         titleFont:(UIFont *)font
                        titleColor:(UIColor *)titleColor
                            normal:(NSString *)normalImage
                         highlight:(NSString *)highLightIamge
                          selected:(NSString *)selectedImage
                          selector:(SEL)selector
                            target:(id)target
{
    UIButton *button = [MAUiFactory createButtonWithRect:frame normal:normalImage highlight:highLightIamge selector:selector target:target];
    button.frame = frame;
    if (title != nil)
        [button setTitle:title forState:UIControlStateNormal];
    
    if (titleColor != nil)
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    if (font != nil)
        [button.titleLabel setFont:font];
    
    if (selectedImage != nil)
        [button setBackgroundImage:[UIImage imageNamed:selectedImage]
                          forState:UIControlStateSelected];
    
    return button;
}


//设置Button(frame + 图标 + 图标的边距 + 响应方法)
+ (void)setButton:(UIButton *)button
         WithRect:(CGRect)frame
      normalImage:(NSString *)normalImage
   highlightImage:(NSString *)highlightImage
  imageEdgeInsets:(UIEdgeInsets)imageEdgeInsets
         selector:(SEL)selector
           target:(id)target
{
    if (button != nil) {
        
        button.frame = frame;
        if (normalImage) {
            [button setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
        }
        
        if (highlightImage) {
            [button setImage:[UIImage imageNamed:highlightImage] forState:UIControlStateHighlighted];
        }
        
        [button setImageEdgeInsets:imageEdgeInsets];
        
        if ((target != nil) && (selector != nil)) {
            [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
}



//设置Button(frame + 背景 + 响应方法)
+ (void)setButton:(UIButton *)button
         WithRect:(CGRect)frame
backgroundImageNormal:(NSString *)backgroundImageNormal
backgroundImageHighLight:(NSString *)backgroundImageHighLight
         selector:(SEL)selector
           target:(id)target
{
    if (button != nil) {
        
        button.frame = frame;
        if (backgroundImageNormal) {
            [button setBackgroundImage:[UIImage imageNamed:backgroundImageNormal] forState:UIControlStateNormal];
        }
        
        if (backgroundImageHighLight) {
            [button setBackgroundImage:[UIImage imageNamed:backgroundImageHighLight] forState:UIControlStateHighlighted];
        }
        
        if ((target != nil) && (selector != nil)) {
            [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
}





//Button(标题+图标+背景)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                             title:(NSString *)title
                         titleFont:(UIFont  *)font
                  titleColorNormal:(UIColor *)titleColorNormal
               titleColorHighLight:(UIColor *)titleColorHighLight
                       normalImage:(NSString *)normalImage
                    highlightImage:(NSString *)highlightImage
             backgroundImageNormal:(NSString *)backgroundImageNormal
          backgroundImageHighLight:(NSString *)backgroundImageHighLight
                          selector:(SEL)selector
                            target:(id)target
{
    UIButton *button = [MAUiFactory createButtonWithRect:frame normal:normalImage highlight:highlightImage selector:selector target:target];
    
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    
    if (font != nil) {
        [button.titleLabel setFont:font];
    }
    
    if (titleColorNormal != nil) {
        [button setTitleColor:titleColorNormal
                     forState:UIControlStateNormal];
    }
    
    if (titleColorHighLight != nil) {
        [button setTitleColor:titleColorHighLight
                     forState:UIControlStateHighlighted];
    }
    
    if (backgroundImageNormal != nil) {
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageNormal]
                          forState:UIControlStateNormal];
    }
    
    if (backgroundImageHighLight != nil) {
        [button setBackgroundImage:[UIImage imageNamed:backgroundImageHighLight]
                          forState:UIControlStateHighlighted];
    }
    return button;
}

#pragma mark alert
+ (void)showAlert:(NSString *)message
{
    UIAlertView *view =  [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    [view show];
}

+(UIScrollView *)createScrollWithRect:(CGRect)frame
                               target:(id)target{
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:frame];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = target;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.backgroundColor = [UIColor blackColor];
    return scrollView;
}

@end
