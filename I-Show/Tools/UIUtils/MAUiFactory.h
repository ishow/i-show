//
//  MAUiFactory.h
//  公用类
//
//  Created by 哈 哈 on 14-10-14.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAUiFactory : NSObject
+(MAUiFactory *)sharedInstance;
#pragma mark label
+ (void)setLable:(UILabel *)label
            font:(UIFont *)font
           title:(NSString *)title;

+ (void)setLable:(UILabel *)label
    numberOfLine:(NSInteger)numberOfLine;

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text;

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text
      backgroundColor:(UIColor *)backgroundColor;

+ (id)createLabelWith:(CGRect)frame
                 text:(NSString *)text
                 font:(UIFont *)font
            textColor:(UIColor *)textColor
      backgroundColor:(UIColor *)backgroundColor;

#pragma mark uitextfield
+ (id)createTextFieldWith:(CGRect)frame
              placeholder:(NSString *)placeholder;

+ (id)createTextFieldWith:(CGRect)frame
                 delegate:(id<UITextFieldDelegate>)delegate
            returnKeyType:(UIReturnKeyType)returnKeyType
          secureTextEntry:(BOOL)secureTextEntry
              placeholder:(NSString *)placeholder
                     font:(UIFont *)font;



+ (id)createTextFieldWithRect:(CGRect)frame
                 keyboardType:(UIKeyboardType)keyboardType
                       secure:(BOOL)secure
                  placeholder:(NSString *)placeholder
                         font:(UIFont *)font
                        color:(UIColor *)color
                     delegate:(id<UITextFieldDelegate>)delegate;

#pragma mark uibutton
//Button(只有背景图)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                            normal:(NSString *)normalImage
                         highlight:(NSString *)clickIamge
                          selector:(SEL)selector
                            target:(id)target;

//设置Button(frame + 图标 + 图标的边距 + 响应方法)
+ (void)setButton:(UIButton *)button
         WithRect:(CGRect)frame
      normalImage:(NSString *)normalImage
   highlightImage:(NSString *)highlightImage
  imageEdgeInsets:(UIEdgeInsets)imageEdgeInsets
         selector:(SEL)selector
           target:(id)target;




//设置Button(frame + 背景 + 响应方法)
+ (void)setButton:(UIButton *)button
         WithRect:(CGRect)frame
backgroundImageNormal:(NSString *)backgroundImageNormal
backgroundImageHighLight:(NSString *)backgroundImageHighLight
         selector:(SEL)selector
           target:(id)target;


//Button(标题+背景图)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                             title:(NSString *)title
                         titleFont:(UIFont *)font
                        titleColor:(UIColor *)titleColor
                            normal:(NSString *)normalImage
                         highlight:(NSString *)highLightIamge
                          selected:(NSString *)clickIamge
                          selector:(SEL)selector
                            target:(id)target;


//Button(标题+图标+背景)
+ (UIButton *)createButtonWithRect:(CGRect)frame
                             title:(NSString *)title
                         titleFont:(UIFont  *)font
                  titleColorNormal:(UIColor *)titleColorNormal
               titleColorHighLight:(UIColor *)titleColorHighLight
                       normalImage:(NSString *)normalImage
                    highlightImage:(NSString *)highlightImage
             backgroundImageNormal:(NSString *)backgroundImageNormal
          backgroundImageHighLight:(NSString *)backgroundImageHighLight
                          selector:(SEL)selector
                            target:(id)target;

#pragma mark Alert提示
+ (void)showAlert:(NSString *)message;
+(UIScrollView *)createScrollWithRect:(CGRect)frame
                               target:(id)target;
@end
