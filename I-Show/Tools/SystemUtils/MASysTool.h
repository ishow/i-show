//
//  MASysTool.h
//  公用类
//
//  Created by 哈 哈 on 14-9-29.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MASysTool : NSObject
//发送邮件
+ (void)sendMail:(NSString *)mail;
//打电话
+ (void)makePhoneCall:(NSString *)tel;
//发短信
+ (void)sendSMS:(NSString *)tel;
//打开URL
+ (void)openURLWithSafari:(NSString *)url;
//计算字符个数
+ (int)countWords:(NSString *)s;

//截屏功能
+(UIImage *)screenshotWithView:(UIView *)view;
+(UIImage *)fullScreenshots;

+ (NSString *) freeDiskSpaceInBytes;

+(BOOL)isCurSysVersion:(NSString *)version;
+(long)curSysVersion;
@end
