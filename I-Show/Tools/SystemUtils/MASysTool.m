//
//  MASysTool.m
//  公用类
//
//  Created by 哈 哈 on 14-9-29.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MASysTool.h"
#include <sys/stat.h>
#include <dirent.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <sys/param.h>
#include <sys/mount.h>

@implementation MASysTool

#pragma mark 发送邮件
+ (void)sendMail:(NSString *)mail {
    mail = [NSString stringWithFormat:@"mailto://%@",mail];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mail]];
}

#pragma mark 打电话
+ (void)makePhoneCall:(NSString *)tel {
//    tel = [NSString stringWithFormat:@"tel://%@",tel];
    tel = [NSString stringWithFormat:@"telprompt://%@",tel];//此方法会询问是否呼叫
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

#pragma mark 发短信
+ (void)sendSMS:(NSString *)tel {
    tel = [NSString stringWithFormat:@"sms://%@",tel];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

#pragma mark 打开URL
+ (void)openURLWithSafari:(NSString *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}


#pragma mark 计算字符个数
+ (int)countWords:(NSString *)s {
    int asciiLength = 0;
    for (int i = 0; i < [s length]; i++)
    {
        unichar uc = [s characterAtIndex: i];
        
        asciiLength += isascii(uc) ? 1 : 2;
    }
    return asciiLength;
}

#pragma mark 根据视图截图
+(UIImage *)screenshotWithView:(UIView *)view{
    UIGraphicsBeginImageContext(view.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //保存到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    return image;
}

#pragma mark 全屏截图
+(UIImage *)fullScreenshots{
    UIWindow *screenWindow = [[UIApplication sharedApplication] keyWindow];
    UIGraphicsBeginImageContext(screenWindow.frame.size);//全屏截图，包括window
    
    [screenWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    //保存到相册
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    return image;
}

#pragma mark 手机剩余存储空间
+ (NSString *) freeDiskSpaceInBytes{
    struct statfs buf;
    long long freespace = -1;
    if(statfs("/var", &buf) >= 0){
        freespace = (long long)(buf.f_bsize * buf.f_bfree);
    }
    return [NSString stringWithFormat:@"手机剩余存储空间为：%qi MB" ,freespace/1024/1024];
}

#pragma 判断是否为指定的系统
+(BOOL)isCurSysVersion:(NSString *)version{
    return ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] != NSOrderedAscending);
}

+(long)curSysVersion{
    return [[[UIDevice currentDevice] systemVersion] longLongValue];
}
@end
