//
//  CXTServiceHandler.h
//  新车行通
//
//  Created by liouly on 14-12-10.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MARequest.h"
#import "MANetAction.h"
#import "HttpDefine.h"

@interface CXTServiceHandler : NSObject

+(void)httpSendWithRequest:(MARequest *)request CallBack:(void(^)(BOOL success,NSError *error))callback;

+(id)handlerObjectFromDic:(NSDictionary *)responseDic;

@end
