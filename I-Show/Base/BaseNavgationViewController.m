//
//  BaseNavgationViewController.m
//  车行通
//
//  Created by liouly on 14-12-8.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//

#import "BaseNavgationViewController.h"

@interface BaseNavgationViewController ()

@end

@implementation BaseNavgationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
            [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_bar_128.png"] forBarMetrics:UIBarMetricsDefault];
        }else{
            [self.navigationBar drawRect:self.navigationBar.bounds];
        }
        self.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:20],UITextAttributeFont,[UIColor whiteColor],UITextAttributeTextColor, nil];
//        self.navigationBar.tintColor = [UIColor colorWithRed:255/255.0 green:122/255.0 blue:123/255.0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

@implementation UINavigationBar (SetBackground)

-(void)drawRect:(CGRect)rect
{
    UIImage *image = [UIImage imageNamed:@"bg_bar_128.png"];
    [image drawInRect:rect];
}

@end
