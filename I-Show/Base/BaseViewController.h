//
//  BaseViewController.h
//  车行通
//
//  Created by liouly on 14-12-8.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface BaseViewController : UIViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

-(void)initView;

-(void)initData;

-(void)setTitle:(NSString *)title;

-(void)popViewController;

@end
