//
//  CustomRefreshTableView.m
//  微考勤
//
//  Created by liouly on 14-9-3.
//  Copyright (c) 2014年 ___AutoNavi___. All rights reserved.
//

#import "CustomRefreshTableView.h"
#import "BaseCell.h"
#import "EGORefreshTableFooterView.h"
#import "EGORefreshTableHeaderView.h"

@implementation CustomRefreshTableView

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:style];
        _tableView.separatorColor = default_blue_color;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
        
        UIView *footView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableFooterView = footView;
        
    }
    return self;
}

-(void)reloadData
{
    [_tableView reloadData];
}

#pragma mark - TableViewDelegate TableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 50;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableView:heightForRowAtIndexPath:)]) {
        
        height = [self.delegate customRefreshTableView:self heightForRowAtIndexPath:indexPath];
        
    }
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 0;
    
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(customRefreshTableView:numberOfRowsInSection:)]) {
        
        count = [self.dataSource customRefreshTableView:self numberOfRowsInSection:section];
        
    }
    
    return count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = 0 ;
    
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberOfSectionsInCustomRefreshTableView:)]) {
        
        count = [self.dataSource numberOfSectionsInCustomRefreshTableView:self];
        
    }
    
    return count;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSArray *titles = nil;
    
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(sectionIndexTitlesForCustomRefreshTableView:)]) {
        
        titles = [NSArray arrayWithArray:[self.dataSource sectionIndexTitlesForCustomRefreshTableView:self]];
        
    }
    
    return titles;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableView:viewForHeaderInSection:)]) {
        
        headerView = [self.delegate customRefreshTableView:self viewForHeaderInSection:section];
        
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableView:heightForHeaderInSection:)]) {
        
        height = [self.delegate customRefreshTableView:self heightForHeaderInSection:section];
        
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CustomRefreshTableView";
    
    UITableViewCell *cell = nil;
    
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(customRefreshTableView:cellForRowAtIndexPath:)]) {
        
        cell = [self.dataSource customRefreshTableView:self cellForRowAtIndexPath:indexPath];
        
    }

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableView:didSelectRowAtIndexPath:)]) {
        
        [self.delegate customRefreshTableView:self didSelectRowAtIndexPath:indexPath];
        
    }

}

#pragma mark - 下拉刷新 上拉加载更多

-(void)setHeaderView
{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
	_refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:
                          CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height,
                                     _tableView.bounds.size.width, _tableView.bounds.size.height)];
    _refreshHeaderView.delegate = (id)self;
    
	[_tableView addSubview:_refreshHeaderView];
    
    [_refreshHeaderView refreshLastUpdatedDate];
}

-(void)removeHeaderView{
    if (_refreshHeaderView && [_refreshHeaderView superview]) {
        [_refreshHeaderView removeFromSuperview];
    }
    _refreshHeaderView = nil;
}

-(void)setFooterView{
    // if the footerView is nil, then create it, reset the position of the footer
    CGFloat height = MAX(_tableView.contentSize.height, _tableView.frame.size.height);
    if (_refreshFooterView && [_refreshFooterView superview]) {
        // reset position
        _refreshFooterView.frame = CGRectMake(0.0f,
                                              height,
                                              _tableView.frame.size.width,
                                              _tableView.bounds.size.height);
    }else {
        // create the footerView
        _refreshFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:
                              CGRectMake(0.0f, height,
                                         _tableView.frame.size.width, _tableView.bounds.size.height)];
        _refreshFooterView.delegate = (id)self;
        [_tableView addSubview:_refreshFooterView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView refreshLastUpdatedDate];
    }
}

//下拉刷新
-(void)refreshView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableViewDidRefreshHeaderView:)]) {
        
        [self.delegate customRefreshTableViewDidRefreshHeaderView:self];
        
    }
}

//上拉加载更多
-(void)getNextPageView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableViewDidRefreshFooterView:)]) {
        
        [self.delegate customRefreshTableViewDidRefreshFooterView:self];
        
    }
}

//移除上拉刷新view
-(void)removeFooterView{
    if (_refreshFooterView && [_refreshFooterView superview]) {
        [_refreshFooterView removeFromSuperview];
    }
    _refreshFooterView = nil;
}

#pragma mark force to show the refresh headerView
-(void)showRefreshHeader:(BOOL)animated
{
	if (animated)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.2];
		_tableView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
        // scroll the table view to the top region
        [_tableView scrollRectToVisible:CGRectMake(0, 0.0f, 1, 1) animated:NO];
        [UIView commitAnimations];
	}
	else
	{
        _tableView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
		[_tableView scrollRectToVisible:CGRectMake(0, 0.0f, 1, 1) animated:NO];
	}
    
    [_refreshHeaderView setState:EGOOPullRefreshLoading];

}

#pragma mark data reloading methods that must be overide by the subclass 刷新delegate

-(void)beginToReloadData:(EGORefreshPos)aRefreshPos{
	
	//  should be calling your tableviews data source model to reload
	_reloading = YES;
    
    if (aRefreshPos == EGORefreshHeader) {
        // pull down to refresh data
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.0];
        
    }else if(aRefreshPos == EGORefreshFooter){
        // pull up to load more data
        [self performSelector:@selector(getNextPageView) withObject:nil afterDelay:0.0];
    }
    
	// overide, the actual loading data operation is done in the subclass
}

- (void)finishReloadingData{
	
	//  model should call this when its done loading
	_reloading = NO;
    
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
    }
    
    if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
        [self setFooterView];
    }
    
    // overide, the actula reloading tableView operation and reseting position operation is done in the subclass
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
	
	if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	if (_refreshHeaderView) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
	
	if (_refreshFooterView) {
        [_refreshFooterView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}

#pragma mark EGORefreshTableDelegate Methods

- (void)egoRefreshTableDidTriggerRefresh:(EGORefreshPos)aRefreshPos{
	
	[self beginToReloadData:aRefreshPos];
}

- (BOOL)egoRefreshTableDataSourceIsLoading:(UIView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}


// if we don't realize this method, it won't display the refresh timestamp
- (NSDate*)egoRefreshTableDataSourceLastUpdated:(UIView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

-(void)finishedLoadData{
    
    [self finishReloadingData];
    
    //移除加载更多
    [self removeFooterView];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(customRefreshTableViewDidfinishedLoadData:)]) {
        
        [self.delegate customRefreshTableViewDidfinishedLoadData:self];
        
    }
    
}


@end
