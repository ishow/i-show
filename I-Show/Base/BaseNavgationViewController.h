//
//  BaseNavgationViewController.h
//  车行通
//
//  Created by liouly on 14-12-8.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavgationViewController : UINavigationController

@end

#pragma mark - 自定义NavigationBar背景

@interface UINavigationBar (SetBackground)

-(void)drawRect:(CGRect)rect;

@end
