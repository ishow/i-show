//
//  BaseModel.h
//  车行通
//
//  Created by liouly on 14-12-8.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface BaseModel : JSONModel <NSCoding>

- (void)encodeWithCoder:(NSCoder *)aCoder;

- (id)initWithCoder:(NSCoder *)aDecoder;

@end
