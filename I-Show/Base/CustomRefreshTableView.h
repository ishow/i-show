//
//  CustomRefreshTableView.h
//  微考勤
//
//  Created by liouly on 14-9-3.
//  Copyright (c) 2014年 ___AutoNavi___. All rights reserved.
//

#import "BaseView.h"
@class EGORefreshTableFooterView;
@class EGORefreshTableHeaderView;
@class CustomRefreshTableView;


@protocol CustomRefreshTableViewDelegate <NSObject>

@optional

- (CGFloat)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

-(void)customRefreshTableViewDidRefreshHeaderView:(CustomRefreshTableView *)customRefreshTableView;

-(void)customRefreshTableViewDidRefreshFooterView:(CustomRefreshTableView *)customRefreshTableView;

-(void)customRefreshTableViewDidfinishedLoadData:(CustomRefreshTableView *)customRefreshTableView;

- (CGFloat)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView heightForHeaderInSection:(NSInteger)section;

- (UIView *)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView viewForHeaderInSection:(NSInteger)section;

@end


@protocol CustomRefreshTableViewDataSource<NSObject>

@required

- (NSInteger)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView numberOfRowsInSection:(NSInteger)section;

- (UITableViewCell *)customRefreshTableView:(CustomRefreshTableView *)customRefreshTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (NSInteger)numberOfSectionsInCustomRefreshTableView:(CustomRefreshTableView *)customRefreshTableView;

-(NSArray *)sectionIndexTitlesForCustomRefreshTableView:(CustomRefreshTableView *)customRefreshTableView;

@end


@interface CustomRefreshTableView : BaseView<UITableViewDataSource,UITableViewDelegate>
{
    //EGOHeader
    EGORefreshTableHeaderView *_refreshHeaderView;      //下拉刷新
    //EGOFooter
    EGORefreshTableFooterView *_refreshFooterView;      //上拉刷新更多
    
    BOOL _reloading;                                    //刷新状态
}

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,weak) id<CustomRefreshTableViewDelegate>delegate;
@property (nonatomic,weak) id<CustomRefreshTableViewDataSource>dataSource;

-(void)setHeaderView;

-(void)setFooterView;

-(void)removeFooterView;

-(void)showRefreshHeader:(BOOL)animated;

-(void)reloadData;

-(void)finishedLoadData;

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style;

@end


