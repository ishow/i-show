//
//  BaseTabBarViewController.m
//  拍卖天下
//
//  Created by admin on 14-2-28.
//  Copyright (c) 2014年 liouly. All rights reserved.
//

#import "BaseTabBarViewController.h"
#import "MainViewController.h"
#import "ADDViewController.h"
#import "BonusesViewController.h"
#import "MineViewController.h"

@interface BaseTabBarViewController ()

@end

@implementation BaseTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    MainViewController *viewCtrl1 = [[MainViewController alloc] init];
    BaseNavgationViewController *navi1 = [[BaseNavgationViewController alloc] initWithRootViewController:viewCtrl1];
    UITabBarItem *item1 = [[UITabBarItem alloc] initWithTitle:@"首页" image:[UIImage imageNamed:@"icon_main_nor"] tag:1001];
    viewCtrl1.navigationItem.title = @"艾秀";
    navi1.tabBarItem = item1;
    
    ADDViewController *viewCtrl2 = [[ADDViewController alloc] init];
    BaseNavgationViewController *navi2 = [[BaseNavgationViewController alloc] initWithRootViewController:viewCtrl2];
    UITabBarItem *item2 = [[UITabBarItem alloc] initWithTitle:@"添加" image:[UIImage imageNamed:@"icon_add_nor"] tag:1002];
    viewCtrl2.navigationItem.title = @"添加";
    navi2.tabBarItem = item2;
    
    BonusesViewController *viewCtrl3 = [[BonusesViewController alloc] init];
    BaseNavgationViewController *navi3 = [[BaseNavgationViewController alloc] initWithRootViewController:viewCtrl3];
    UITabBarItem *item3 = [[UITabBarItem alloc] initWithTitle:@"红包" image:[UIImage imageNamed:@"icon_redbao_nor"] tag:1003];
    viewCtrl3.navigationItem.title = @"红包";
    navi3.tabBarItem = item3;
    
    MineViewController *viewCtrl4 = [[MineViewController alloc] init];
    BaseNavgationViewController *navi4 = [[BaseNavgationViewController alloc] initWithRootViewController:viewCtrl4];
    UITabBarItem *item4 = [[UITabBarItem alloc] initWithTitle:@"我的" image:[UIImage imageNamed:@"icon_mine_nor"] tag:1004];
    viewCtrl4.navigationItem.title = @"个人信息";
    navi4.tabBarItem = item4;
    
    NSArray *navArray = [NSArray arrayWithObjects:navi1,navi2,navi3,navi4,nil];
    
    self.viewControllers = navArray;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
