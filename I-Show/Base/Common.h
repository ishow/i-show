//
//  DevCommon.h
//  CameraTest
//
//  Created by liouly on 13-4-21.
//  Copyright (c) 2013年 Fructivity. All rights reserved.
//

#ifndef Common_h
#define Common_h

#define NavigationBar_HEIGHT ([[UIDevice currentDevice] systemVersion].floatValue>=7.0?64:44)

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define SAFE_RELEASE(x) [x release];x=nil
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion ([[UIDevice currentDevice] systemVersion])
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

#define BACKGROUND_COLOR [UIColor colorWithRed:242.0/255.0 green:236.0/255.0 blue:231.0/255.0 alpha:1.0]

//use dlog to print while in debug model
#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)
#endif


#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

/* 导航栏高度 */
#define kNavigationBarHeight 44
#define kNavigationBarHeight_IOS7 64

/* 工具栏高度 */
#define kTabBarHeight        49

/* 获取系统信息 */
#define kSystemVersion   [[UIDevice currentDevice] systemVersion]

/* 获取当前语言环境 */
#define kCurrentLanguage [[NSLocale preferredLanguages] objectAtIndex:0]

/* 获取当前APP版本 */
#define kAppVersion      [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

/* 获取当前APP名字 */
#define APP_NAME            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]

/* 是否为7以上版本 */
#define SYSTEM7_OR_LATER	( [[[UIDevice currentDevice] systemVersion] compare:@"7."] != NSOrderedAscending )


#if TARGET_OS_IPHONE
//iPhone Device
#endif

#if TARGET_IPHONE_SIMULATOR
//iPhone Simulator
#endif


//ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif


//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block)


#define USER_DEFAULT [NSUserDefaults standardUserDefaults]
#define ImageNamed(_pointer) [UIImage imageNamed:[UIUtil imageName:_pointer]]



#pragma mark - common functions
#define RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil; }


#pragma mark - degrees/radian functions
#define degreesToRadian(x) (M_PI * (x) / 180.0)
#define radianToDegrees(radian) (radian*180.0)/(M_PI)

#pragma mark - color functions
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define ITTDEBUG
#define ITTLOGLEVEL_INFO     10
#define ITTLOGLEVEL_WARNING  3
#define ITTLOGLEVEL_ERROR    1

#ifndef ITTMAXLOGLEVEL

#ifdef DEBUG
#define ITTMAXLOGLEVEL ITTLOGLEVEL_INFO
#else
#define ITTMAXLOGLEVEL ITTLOGLEVEL_ERROR
#endif

#endif

// The general purpose logger. This ignores logging levels.
#ifdef ITTDEBUG
#define ITTDPRINT(xx, ...)  NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define ITTDPRINT(xx, ...)  ((void)0)
#endif

// Prints the current method's name.
#define ITTDPRINTMETHODNAME() ITTDPRINT(@"%s", __PRETTY_FUNCTION__)

// Log-level based logging macros.
#if ITTLOGLEVEL_ERROR <= ITTMAXLOGLEVEL
#define ITTDERROR(xx, ...)  ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDERROR(xx, ...)  ((void)0)
#endif

#if ITTLOGLEVEL_WARNING <= ITTMAXLOGLEVEL
#define ITTDWARNING(xx, ...)  ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDWARNING(xx, ...)  ((void)0)
#endif

#if ITTLOGLEVEL_INFO <= ITTMAXLOGLEVEL
#define ITTDINFO(xx, ...)  ITTDPRINT(xx, ##__VA_ARGS__)
#else
#define ITTDINFO(xx, ...)  ((void)0)
#endif

#ifdef ITTDEBUG
#define ITTDCONDITIONLOG(condition, xx, ...) { if ((condition)) { \
ITTDPRINT(xx, ##__VA_ARGS__); \
} \
} ((void)0)
#else
#define ITTDCONDITIONLOG(condition, xx, ...) ((void)0)
#endif

#define ITTAssert(condition, ...)                                       \
do {                                                                      \
if (!(condition)) {                                                     \
[[NSAssertionHandler currentHandler]                                  \
handleFailureInFunction:[NSString stringWithUTF8String:__PRETTY_FUNCTION__] \
file:[NSString stringWithUTF8String:__FILE__]  \
lineNumber:__LINE__                                  \
description:__VA_ARGS__];                             \
}                                                                       \
} while(0)



#define _po(o) DLOG(@"%@", (o))
#define _pn(o) DLOG(@"%d", (o))
#define _pf(o) DLOG(@"%f", (o))
#define _ps(o) DLOG(@"CGSize: {%.0f, %.0f}", (o).width, (o).height)
#define _pr(o) DLOG(@"NSRect: {{%.0f, %.0f}, {%.0f, %.0f}}", (o).origin.x, (o).origin.x, (o).size.width, (o).size.height)

#define DOBJ(obj)  DLOG(@"%s: %@", #obj, [(obj) description])

#define MARK    NSLog(@"\nMARK: %s, %d", __PRETTY_FUNCTION__, __LINE__)


//SQLite打印
#define SQLLog(sql)   NSLog(@"************SQL语句************\n\n%@\n\n******************************",sql)

//#define SQLLog(sql)   @""
//#define NSLog(...) @""


//////////单例//////////
#undef	AS_SINGLETON
#define AS_SINGLETON( __class ) \
- (__class *)sharedInstance; \
+ (__class *)sharedInstance;
#undef	DEF_SINGLETON
#define DEF_SINGLETON( __class ) \
- (__class *)sharedInstance \
{ \
return [__class sharedInstance]; \
} \
+ (__class *)sharedInstance \
{ \
static dispatch_once_t once; \
static __class * __singleton__; \
dispatch_once( &once, ^{ __singleton__ = [[[self class] alloc] init]; } ); \
return __singleton__; \
}

#define AppOrginY       ([[UIDevice currentDevice] systemVersion].floatValue>=7.0?64:44)


#define appKeyWindow [UIApplication sharedApplication].keyWindow

//类名
#define selfClassName NSStringFromClass(self.class)

/* 高德地图appkey */
const static NSString *APIKey = @"";

//调起第三方应用专用
#define ApplicationScheme @""

//高德地图下载地址
#define AmapDownloadURL @"http://itunes.apple.com/cn/app//id461703208?mt=8"


/* 常用属性设置 */
#define clean_color [UIColor clearColor]
#define default_gray_color [ToolUtils colorWithHexString:@"#aeaeae"]
#define default_white_color [ToolUtils colorWithHexString:@"#ffffff"]
#define default_blue_color [ToolUtils colorWithHexString:@"#4dc2d4"]
#define default_black_color [ToolUtils colorWithHexString:@"#a8a8a8"]
#define default_deepgray_color [UIColor darkTextColor]
#define default_midgray_color [ToolUtils colorWithHexString:@"#3a3a3a"]
#define default_deepblue_color [ToolUtils colorWithHexString:@"#008397"]
#define textView_placeholder_color [ToolUtils colorWithHexString:@"#cdcdcf"]
#define SectionIndexColor [ToolUtils colorWithHexString:@"#adadad"]
#define ButtonListColor [ToolUtils colorWithHexString:@"#65c7d6"]
#define default_green_color [UIColor greenColor]
#define default_red_color [UIColor redColor]
#define default_red_light_color [UIColor colorWithRed:255/255.0 green:122/255.0 blue:123/255.0 alpha:1]

#define default_font8 [UIFont systemFontOfSize:8]
#define default_font10 [UIFont systemFontOfSize:10]
#define default_font12 [UIFont systemFontOfSize:12]
#define default_font13 [UIFont systemFontOfSize:13]
#define default_font14 [UIFont systemFontOfSize:14]
#define default_font15 [UIFont systemFontOfSize:15]
#define default_font16 [UIFont systemFontOfSize:16]
#define default_font18 [UIFont systemFontOfSize:18]
#define default_font20 [UIFont systemFontOfSize:20]

#define default_font8_bold [UIFont boldSystemFontOfSize:8]
#define default_font10_bold [UIFont boldSystemFontOfSize:10]
#define default_font12_bold [UIFont boldSystemFontOfSize:12]
#define default_font13_bold [UIFont boldSystemFontOfSize:13]
#define default_font14_bold [UIFont boldSystemFontOfSize:14]
#define default_font15_bold [UIFont boldSystemFontOfSize:15]
#define default_font16_bold [UIFont boldSystemFontOfSize:16]
#define default_font18_bold [UIFont boldSystemFontOfSize:18]
#define default_font20_bold [UIFont boldSystemFontOfSize:20]


/* 快捷操作 */
#define head_default1x [UIImage imageNamed:@"icon_head_default.png"]
#define head_default2x [UIImage imageNamed:@"icon_head_defaultb.png"]
#define head_defaultn [UIImage imageNamed:@"icon_head_defaultn.png"]
#define placeholder_144 [UIImage imageNamed:@"placeholder_144.png"]



/* 网络请求接口 */

//ip:port

//#define BASEURL @"http://192.168.1.111:8080/components_platform/"
#define BASEURL @"http://121.8.182.98:8091/base/"            //公司测试地址

//登录
#define LOGIN [NSString stringWithFormat:@"%@%@",BASEURL,@"app/loginApp"]

#endif

