//
//  CustomAlertView.h
//  微考勤
//
//  Created by liouly on 14-9-18.
//  Copyright (c) 2014年 ___AutoNavi___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIAlertView

@property (nonatomic,strong) NSString *indetifier;
@property (nonatomic,strong) id info;

@end
