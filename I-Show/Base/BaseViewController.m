
//
//  BaseViewController.m
//  车行通
//
//  Created by liouly on 14-12-8.
//  Copyright (c) 2014年 ___liouly___. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        [self initData];
        
    }
    
    return self;
}

-(void)setViewBackgroundColor
{
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setViewBackgroundColor];
    
//    UIButton *backBtn = [MAUiFactory createButtonWithRect:CGRectMake(0, 0, 44, 44) normal:@"" highlight:@"" selector:@selector(backHandler) target:self];
//    
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
//    self.navigationItem.leftBarButtonItem = backItem;
    
    
    [self initView];
    
}

-(void)backHandler
{
    
}

-(void)initData
{
    
}

-(void)initView
{
    
}

-(void)setTitle:(NSString *)title
{

}

-(void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
