//
//  MAAppStoreInfo.h
//  http2.0
//
//  Created by 哈 哈 on 14-10-12.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MAModel.h"

@interface MAAppStoreInfo : MAModel
@property(nonatomic,strong)NSString *trackViewUrl;//下载地址
@property(nonatomic,strong)NSString *version;//版本号
@property(nonatomic,strong)NSString *trackId;//ID号
@end
