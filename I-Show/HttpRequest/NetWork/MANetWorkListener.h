//
//  MANetWorkListener.h
//  http2.0
//  网络连接的监听
//  Created by 哈 哈 on 14-10-12.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>
//网络状态监听
#define MANetWorkListenerNotification         @"NetWorkListenerNotification"
//网络连接的类型
typedef enum {
    MANetWorkStateNot       = 0,
    MANetWorkStateWiFi      = 1,
    MANetWorkStateWWAN      = 2
}MANetWorkState;
@interface MANetWorkListener : NSObject

@property (readonly, nonatomic, assign, getter = isReachable) BOOL reachable;

@property (readonly, nonatomic, assign, getter = isReachableViaWWAN) BOOL reachableViaWWAN;

@property (readonly, nonatomic, assign, getter = isReachableViaWiFi) BOOL reachableViaWiFi;

@property (readwrite, nonatomic,assign)NSInteger netWorkState;


+(MANetWorkListener *)sharedInstance;
-(void)startMonitor;
@end
