//
//  MANetAction.m
//  http2.0
//
//  Created by 哈 哈 on 14-10-10.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MANetAction.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "XMLDictionary.h"
#import "UIProgressView+AFNetworking.h"
#include <objc/runtime.h>
#import "CXTServiceHandler.h"

#define HOST_URL @"admin.maichong.me" //服务端域名:端口

@interface MANetAction()
@property(nonatomic,strong)AFHTTPRequestOperationManager *httpManager;
@property(nonatomic,strong)AFURLSessionManager *sessionManager;
@end

@implementation MANetAction

+(MANetAction *)sharedInstance{
    static MANetAction *sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[MANetAction alloc]init];
    });
    return sharedManager;
    
}

-(id)init{
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark 发送请求
- (AFHTTPRequestOperation *)send:(MARequest *)request andCallBack:(void(^)(BOOL success,NSError *error))callback{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    if (!_httpManager) {
        _httpManager = [AFHTTPRequestOperationManager manager];
    }
    if([request.MENTHOD isEqualToString:@"GET"]){
        return [self get:request andCallBack:callback];
    }else{
        return [self post:request andCallBack:callback];
    }
}

#pragma mark get请求
- (AFHTTPRequestOperation *)get:(MARequest *)request andCallBack:(void(^)(BOOL success,NSError *error))callback{
    NSString *url = [self urlWithRequest:request];
    id requestParams;
    if (request.requestParams) {
        
        requestParams = request.requestParams;

    }
    //由于要自动解析，默认返回文本格式
    _httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    /*
    if (request.responseType == MAJsonType) {
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/json"];
    }else if(request.responseType == MAXmlType){
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/xml"];
    }else{
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
     */
    AFHTTPRequestOperation *op = [_httpManager GET:url parameters:requestParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self resultWithObj:responseObject andRequest:request];
        //处理结果
        callback(true,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Could not connect to the server
        //Request failed: unacceptable content-type: text/xml
        //A server with the specified hostname could not be found.
        [self errorResult:error andCallBack:callback];
    }] ;
    request.url = op.request.URL.absoluteString;
    return op;
}


#pragma mark POST请求
- (AFHTTPRequestOperation *)post:(MARequest *)request andCallBack:(void(^)(BOOL success,NSError *error))callback{
    NSString *url = [self urlWithRequest:request];
    NSDictionary *requestParams = nil;
    if (request.requestParams) {
        requestParams = request.requestParams;
    }
    //设定返回值的序列
    _httpManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFHTTPRequestOperation *op = [_httpManager POST:url parameters:requestParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //处理结果
        [self resultWithObj:responseObject andRequest:request];
        callback(YES,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self errorResult:error andCallBack:callback];
    }];
    request.url = op.request.URL.absoluteString;
    return op;
}

#pragma mark 下载
-(NSURLSessionTask *)download:(MARequest *)request{
    NSString *url = [self urlWithRequest:request];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    if (!_sessionManager) {
        _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [_sessionManager downloadTaskWithRequest:urlRequest progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
    }];
    
    [_sessionManager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"%lld----%lld-----%lld",bytesWritten,totalBytesWritten,totalBytesExpectedToWrite);
    }];
    //设置进度条
//    if (request.progressView) {
//        [request.progressView setProgressWithDownloadProgressOfTask:downloadTask animated:YES];
//    }
//    [downloadTask resume];
    return downloadTask;
}

#pragma mark 断点下载
-(NSURLSessionTask *)downloadWithData:(NSData *)data request:(MARequest *)request{
    NSString *url = [self urlWithRequest:request];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    if (!_sessionManager) {
        _sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    
    NSURL *URL = [NSURL URLWithString:url];
    __unused NSURLRequest *urlRequest = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [_sessionManager downloadTaskWithResumeData:data progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
    }];
    [_sessionManager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"%lld----%lld-----%lld",bytesWritten,totalBytesWritten,totalBytesExpectedToWrite);
    }];
    //设置进度条
//    if (request.progressView) {
//        [request.progressView setProgressWithDownloadProgressOfTask:downloadTask animated:YES];
//    }
//    [downloadTask resume];
    return downloadTask;
}

#pragma mark 组装URL
-(NSString *)urlWithRequest:(MARequest *)request{
    if (request.url&&request.url.length>0) {
        return request.url;
    }
    NSString *url = @"";
    if(request.SCHEME.length == 0){
        url = [NSString stringWithFormat:@"http://"];
    }else{
        url = [NSString stringWithFormat:@"%@://",request.SCHEME];
    }
    
    if(request.HOST.length == 0){
        url = [NSString stringWithFormat:@"%@%@%@",url,HOST_URL,request.PATH];
    }else{
        url = [NSString stringWithFormat:@"%@%@%@",url,request.HOST,request.PATH];
    }

    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return url;
}

#pragma mark 处理成功的结果
-(void)resultWithObj:(id)responseObject andRequest:(MARequest *)request {
    if(request.responseType == MAFileType){
        request.outputData = responseObject;
        return;
    }
    
    NSString *result = [[NSString alloc] initWithData:responseObject  encoding:NSUTF8StringEncoding];

    if (request.responseType == MATextType) {
        request.responseString = result;
    }else if (request.responseType == MAJsonType){//JSON解析
        request.output = [self.class dictionaryWithJsonString:responseObject];
        
        return;
        
    }else  if (request.responseType == MAXmlType){//XML要转成NSDictionary
        //XMLDictionary比XMLReader好用些，XMLReader解析出来的值前面加上了.text
//        NSError *parseError = nil;
//        NSDictionary *dictionary = [XMLReader dictionaryForXMLData:responseObject error:&parseError];
        NSDictionary *dic = [NSDictionary dictionaryWithXMLData:responseObject];
        request.output = dic;
        
    }
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

#pragma mark 失败的请求
-(void)errorResult:(NSError *)error andCallBack:(void(^)(BOOL success,NSError *error))callback{
    NSLog(@"失败---%@-------%d",error.localizedDescription,error.code);
    callback(NO,error);
}
#pragma mark 取消请求
-(void)cancelRequest{
    if (_httpManager) {
        NSOperationQueue *queue = _httpManager.operationQueue;
        for(NSOperation *operation in queue.operations){
            [operation cancel];
            [operation setCompletionBlock:nil];
        }
    }
    
}
#pragma mark 取消下载的请求
-(void)cancelDownloadRequest{
    if (_sessionManager) {
        for(NSURLSessionDownloadTask * task in _sessionManager.tasks){
            [task cancelByProducingResumeData:^(NSData *resumeData) {
                
            }];
            
        }
    }
}

#pragma mark 错误结果
- (NSInteger)errorRequest:(NSError *)error
{
    if (error == nil) {
        return MANetActionServerError;
    }
    NSInteger errorResult;
    if ([error.domain isEqualToString:NSURLErrorDomain]) {
        switch (error.code) {
            case NSURLErrorCannotConnectToHost:
            case NSURLErrorCannotFindHost:
            case NSURLErrorNetworkConnectionLost:
            case NSURLErrorDNSLookupFailed:
            case NSURLErrorNotConnectedToInternet:
                errorResult = MANetActionConnectError;
                break;
            case NSURLErrorTimedOut:
                errorResult = MANetActionTimeOut;
                break;
                
            default:
                errorResult = MANetActionConnectError;
                break;
        }
    }
    return errorResult;
}
@end
