//
//  MANetWorkListener.m
//  http2.0
//
//  Created by 哈 哈 on 14-10-12.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MANetWorkListener.h"
#import "AFNetworkReachabilityManager.h"

@implementation MANetWorkListener
+(MANetWorkListener *)sharedInstance{
    static MANetWorkListener *sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[MANetWorkListener alloc]init];
    });
    return sharedManager;
    
}

//开始监测
-(void)startMonitor{
    /**
     AFNetworkReachabilityStatusUnknown          = -1,  // 未知
     AFNetworkReachabilityStatusNotReachable     = 0,   // 无连接
     AFNetworkReachabilityStatusReachableViaWWAN = 1,   // 3G
     AFNetworkReachabilityStatusReachableViaWiFi = 2,   // 局域网络
     */
    // 如果要检测网络状态的变化,必须用检测管理器的单例的startMonitoring
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        self.netWorkState = status;
        [[NSNotificationCenter defaultCenter]postNotificationName:MANetWorkListenerNotification object:nil];
    }];
}

#pragma mark -

- (BOOL)isReachable {
    return [self isReachableViaWWAN] || [self isReachableViaWiFi];
}

- (BOOL)isReachableViaWWAN {
    return self.netWorkState == AFNetworkReachabilityStatusReachableViaWWAN;
}

- (BOOL)isReachableViaWiFi {
    return self.netWorkState == AFNetworkReachabilityStatusReachableViaWiFi;
}
@end
