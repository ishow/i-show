//
//  MARequest.m
//  http2.0
//
//  Created by 哈 哈 on 14-10-10.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "MARequest.h"
#include <objc/runtime.h>

#define kSCHEME         @"http"
#define kHOST           @"121.8.182.98:8091"
#define kPATH           @"/cj/gate?"
#define kMENTHOD        @"GET"

@interface MARequest()


@end

@implementation MARequest

+(id)request
{
    return [[self alloc] initRequest];
}


-(id)initRequest
{
    self = [self init];
    
    if(self){
        
        [self config];
        
    }
    return self;
}

-(void)config
{
    self.output = nil;
    self.SCHEME = kSCHEME;
    self.HOST = kHOST;
    self.PATH = kPATH;
    self.MENTHOD = kMENTHOD;
    self.responseType = MAJsonType;
    self.progressView = nil;
}

-(void)loadRequest
{
    

    
}


@end
