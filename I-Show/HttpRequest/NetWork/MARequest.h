//
//  MARequest.h
//  http2.0
//
//  Created by 哈 哈 on 14-10-10.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    MATextType        =0,
    MAXmlType         =1,
    MAJsonType        =2,
    MAFileType       =3
}MAResponseType;

@interface MARequest : NSObject

@property (nonatomic,retain)NSString *SCHEME;//请求的头类型,http,https,ftp....
@property (nonatomic,retain)NSString *HOST;//主机名
@property (nonatomic,retain)NSString *PATH;//URL访问的路径
@property (nonatomic,retain)NSString *MENTHOD;//请求的方法,get,post,delete...
@property (nonatomic,assign)NSInteger responseType;//返回结果类型
@property (nonatomic,retain)NSString *url;//请求的URL;
@property (nonatomic,retain)NSDictionary *output;//返回结果
@property (nonatomic,retain)NSData *outputData;//返回的数据
@property (nonatomic,retain)NSString *responseString;//返回的字符串
@property (nonatomic,strong)NSDictionary *requestParams;//请求的参数

@property (nonatomic,retain)UIProgressView *progressView;//进度条
@property (nonatomic,assign)NSTimeInterval timeoutInterval;//超时

+(id)request;

@end
