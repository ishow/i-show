//
//  MANetAction.h
//  http2.0
//
//  Created by 哈 哈 on 14-10-10.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "MARequest.h"

typedef enum {
    MANetActionTimeOut = 1,//超时
    MANetActionConnectError = 2,//连接错误
    MANetActionServerError = 3//服务器错误
}MANetActionError;
@interface MANetAction : NSObject

+(MANetAction *)sharedInstance;
- (AFHTTPRequestOperation *)send:(MARequest *) request andCallBack:(void(^)(BOOL success,NSError *error))callback;

-(NSURLSessionTask *)download:(MARequest *)request;
-(NSURLSessionTask *)downloadWithData:(NSData *)data request:(MARequest *)request;
-(void)cancelRequest;
-(void)cancelDownloadRequest;

- (NSInteger)errorRequest:(NSError *)error;
@end
