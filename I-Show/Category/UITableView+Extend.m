//
//  UITableView+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "UITableView+Extend.h"

@implementation UITableView (Extend)

#pragma mark 删除分割线
-(void)hidenCellLine{
    self.separatorColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}
#pragma mark 隐藏多余的分割线
-(void)hidenExtraCellLine
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    [self setTableFooterView:view];
}

#pragma mark 重新加载指定行
- (void)reloadRow:(NSInteger)row inSection:(NSInteger)section
{
    [self reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark 重新加载指定行
- (void)reloadRowWithNoneAnimation:(NSInteger)row
{
    [self reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}
@end
