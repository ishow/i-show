//
//  NSObject+Extend.h
//  CommonLib
//
//  Created by 哈 哈 on 14-12-9.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extend)
-(id)initWithDictionary:(NSDictionary *)dictionary;
@end
