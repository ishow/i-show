//
//  NSDate+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extend)
@property (nonatomic, readonly) NSInteger	year;
@property (nonatomic, readonly) NSInteger	month;
@property (nonatomic, readonly) NSInteger	day;
@property (nonatomic, readonly) NSInteger	hour;
@property (nonatomic, readonly) NSInteger	minute;
@property (nonatomic, readonly) NSInteger	second;
@property (nonatomic, readonly) NSInteger	weekday;

- (NSString *)stringWithDateFormat:(NSString *)format;

- (long long)timeStamp;

-(NSString *)curDateAndTime;

-(NSString *)curDate;

-(NSString *)curTime;

-(long long int)dateTimerWithString:(NSString *)dateStr;

- (NSDate*) stringToNSDate:(NSString*)input;

-(NSDate *)dateByAddingDays:(NSInteger)numDays;

- (NSDate *)dateWithZeroTime;

- (NSDate *)dateWithZeroSeconds;

+ (NSString *)dbFormatString;

+ (NSString *)shortdbFormatString;

+ (NSString *)dayFormatString;

- (NSString *)getChineseWeek:(NSDate *)date;
@end
