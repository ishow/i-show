//
//  NSObject+Extend.m
//  CommonLib
//
//  Created by 哈 哈 on 14-12-9.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "NSObject+Extend.h"
#import <objc/runtime.h>

@implementation NSObject (Extend)

-(id)initWithDictionary:(NSDictionary *)dictionary{
    self = [self init];
    if (self&&dictionary) {
        unsigned int propertyCount = 0;
        objc_property_t *properties = class_copyPropertyList([self class], &propertyCount);
        
        for (unsigned int i = 0; i < propertyCount; ++i) {
            objc_property_t property = properties[i];
            const char * name = property_getName(property);//获取属性名字
            NSString *attributeName = [NSString stringWithFormat:@"%s",name];
            [self setValue:[dictionary valueForKey:attributeName] forKey:attributeName];
        }
    }
    return  self;
}

@end
