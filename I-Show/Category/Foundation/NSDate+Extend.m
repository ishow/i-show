//
//  NSDate+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "NSDate+Extend.h"

@implementation NSDate (Extend)

#pragma mark 获取年
- (NSInteger)year
{
	return [[NSCalendar currentCalendar] components:NSYearCalendarUnit
										   fromDate:self].year;
}

#pragma mark 获取月
- (NSInteger)month
{
	return [[NSCalendar currentCalendar] components:NSMonthCalendarUnit
										   fromDate:self].month;
}

#pragma mark 获取日
- (NSInteger)day
{
	return [[NSCalendar currentCalendar] components:NSDayCalendarUnit
										   fromDate:self].day;
}

#pragma mark 获取小时
- (NSInteger)hour
{
	return [[NSCalendar currentCalendar] components:NSHourCalendarUnit
										   fromDate:self].hour;
}

#pragma mark 获取分
- (NSInteger)minute
{
	return [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit
										   fromDate:self].minute;
}

#pragma mark 获取秒
- (NSInteger)second
{
	return [[NSCalendar currentCalendar] components:NSSecondCalendarUnit
										   fromDate:self].second;
}

#pragma mark 获取星期几
- (NSInteger)weekday
{
	return [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit
										   fromDate:self].weekday;
}

#pragma mark 获取中文的星期
- (NSString *)getChineseWeek:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
    NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    
    NSInteger week = [comps weekday];
    switch (week) {
        case 1:
            return @"星期天";
        case 2:
            return @"星期一";
        case 3:
            return @"星期二";
        case 4:
            return @"星期三";
        case 5:
            return @"星期四";
        case 6:
            return @"星期五";
        case 7:
            return @"星期六";
        default:
            break;
    }
    return nil;
}

#pragma mark 日期格式化
- (NSString *)stringWithDateFormat:(NSString *)format
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:format];
	return [dateFormatter stringFromDate:self];
}

#pragma mark 获取时间值
- (long long)timeStamp
{
	return (long long)[[NSDate date] timeIntervalSince1970];
}

#pragma mark 获取当前日期和时间
-(NSString *)curDateAndTime
{
    return [self stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss"];
}

#pragma mark 获取当前日期
-(NSString *)curDate
{
    return [self stringWithDateFormat:@"yyyy-MM-dd"];
}

#pragma mark 获取当前时间
-(NSString *)curTime
{
    return [self stringWithDateFormat:@"HH:mm:ss"];
}

#pragma mark 根据指定的时间返回秒
-(long long int)dateTimerWithString:(NSString *)dateStr{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* date = [formater dateFromString:dateStr];
    long long int result =[date timeIntervalSince1970];
    return result;
}

#pragma mark 字符转时间 yyyy-MM-dd HH:mm:ss
- (NSDate*) stringToNSDate:(NSString*)input
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSDate dbFormatString]];
    
    NSDate *date = [dateFormatter dateFromString:input];
    
    return date;
}

#pragma mark 字符串转yyyy-MM-dd
- (NSDate*) stringToShortNSDate:(NSString*)input
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:[NSDate dayFormatString]];
    
    NSDate *date = [dateFormatter dateFromString:input];
    
    return date;
}

#pragma mark 多少天后
-(NSDate *)dateByAddingDays:(NSInteger)numDays
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:numDays];
    
    NSDate *date = [gregorian dateByAddingComponents:comps toDate:self options:0];
    return date;
}

#pragma mark 时间都是0
- (NSDate *)dateWithZeroTime
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit;
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	[comps setHour:0];
	[comps setMinute:0];
	[comps setSecond:0];
	return [calendar dateFromComponents:comps];
}

#pragma mark 秒都是0
- (NSDate *)dateWithZeroSeconds
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	[comps setSecond:0];
	return [calendar dateFromComponents:comps];
}

#pragma mark 格式yyyy-MM-dd HH:mm:ss
+ (NSString *)dbFormatString {
	return @"yyyy-MM-dd HH:mm:ss";
}

#pragma mark 格式yyyy-MM-dd HH:mm
+ (NSString *)shortdbFormatString {
	return @"yyyy-MM-dd HH:mm";
}

#pragma mark 格式yyyy-MM-dd
+ (NSString *)dayFormatString {
	return @"yyyy-MM-dd";
}
@end
