//
//  NSData+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "NSData+Extend.h"

@implementation NSData (Extend)

#pragma mark 转成字符串
-(NSString *)string{
    NSString *result = [[NSString alloc] initWithData:self  encoding:NSUTF8StringEncoding];
    return result;
}


@end
