//
//  NSString+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extend)

-(BOOL)isEmpty;

-(BOOL)isNotEmpty;

- (NSData *)data;

- (BOOL) containsString: (NSString*) substring;

-(NSString *)replace:(NSString *)target withString:(NSString *)replacement;

- (NSString *)trim;

- (CGSize)sizeWithFont:(UIFont *)font byWidth:(CGFloat)width;

- (CGSize)sizeWithFont:(UIFont *)font byHeight:(CGFloat)height;

- (CGSize)sizeWithFont:(UIFont *)font width:(CGFloat)width height:(CGFloat)height;

- (int)heightWithFont:(UIFont *)font width:(float)width;

- (int)widthWithFont:(UIFont *)font;
@end
