//
//  NSString+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "NSString+Extend.h"

@implementation NSString (Extend)

#pragma mark 字符串为空
-(BOOL)isEmpty{
    if (self == nil) {
        return YES;
    }
    if ([self rangeOfString:@"(null)"].length>0) {
        return YES;
    }
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 ? YES : NO;
}

#pragma mark 字符串不为空
-(BOOL)isNotEmpty{
    return [self isEmpty] ? NO:YES;
}

#pragma mark 字符串转nsdata
- (NSData *)data
{
	return [self dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
}

#pragma mark 是否包含
- (BOOL) containsString: (NSString*) substring
{
    NSRange range = [self rangeOfString : substring];
	
    BOOL found = ( range.location != NSNotFound );
	
    return found;
}

#pragma mark 替换
-(NSString *)replace:(NSString *)target withString:(NSString *)replacement{
    return [self stringByReplacingOccurrencesOfString:target withString:replacement];
}

#pragma mark 去除空格
- (NSString *)trim
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark 通过宽度获取字符的尺寸
- (CGSize)sizeWithFont:(UIFont *)font byWidth:(CGFloat)width
{
	return [self sizeWithFont:font
			constrainedToSize:CGSizeMake(width, 999999.0f)
				lineBreakMode:NSLineBreakByCharWrapping];
}

#pragma mark 通过高度获取字符的尺寸
- (CGSize)sizeWithFont:(UIFont *)font byHeight:(CGFloat)height
{
	return [self sizeWithFont:font
			constrainedToSize:CGSizeMake(999999.0f, height)
				lineBreakMode:NSLineBreakByCharWrapping];
}

#pragma mark 通过高度宽度获取字符的尺寸
- (CGSize)sizeWithFont:(UIFont *)font width:(CGFloat)width height:(CGFloat)height
{
	return [self sizeWithFont:font
			constrainedToSize:CGSizeMake(width, height)
				lineBreakMode:NSLineBreakByCharWrapping];
}

#pragma mark 根据指定的宽度获取高度
- (int)heightWithFont:(UIFont *)font width:(float)width
{
    return ceilf([self sizeWithFont:font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)].height);
}

#pragma mark 获取宽度
- (int)widthWithFont:(UIFont *)font
{
    return ceilf([self sizeWithFont:font].width);
}
@end
