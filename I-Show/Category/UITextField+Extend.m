//
//  UITextField+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "UITextField+Extend.h"

@implementation UITextField (Extend)

#pragma mark 靠左的宽度
- (void)setContentPaddingLeft:(float)width
{
    self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, self.frame.size.height)];
    self.leftViewMode = UITextFieldViewModeAlways;
}
@end
