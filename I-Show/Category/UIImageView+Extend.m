//
//  UIImageView+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "UIImageView+Extend.h"

@implementation UIImageView (Extend)

- (void) setImageWithName:(NSString *)name {
	[self setImage:[UIImage imageNamed:name]];
	[self sizeToFit];
}

#pragma mark 大图片自适应并居中
-(void)resizeImage{
    //retina屏幕上面，必须要设置
    [self setContentScaleFactor:[[UIScreen mainScreen] scale]];
    //自适合
    self.contentMode =  UIViewContentModeScaleAspectFill;
//    self.autoresizingMask = UIViewAutoresizingFlexibleHeight;//设置了此项会改变高度
    //裁剪
    self.clipsToBounds  = YES;
}
@end
