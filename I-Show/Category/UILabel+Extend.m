//
//  UILabel+Extend.m
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import "UILabel+Extend.h"
#import "NSString+Extend.h"
#import "UIView+Extend.h"

@implementation UILabel (Extend)

#pragma mark 重新设定高度
- (void)resizeToFitHeight
{
    self.numberOfLines = 0;
    self.height = [self.text heightWithFont:self.font width:self.width];
}
@end
