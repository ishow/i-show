//
//  UIImageView+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extend)

- (void) setImageWithName:(NSString *)name;

-(void)resizeImage;
@end
