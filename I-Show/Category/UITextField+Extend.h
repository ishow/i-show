//
//  UITextField+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extend)
- (void)setContentPaddingLeft:(float)width;
@end
