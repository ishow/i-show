//
//  UIAlertView+Extend.h
//  MyUtil
//
//  Created by 哈 哈 on 13-10-4.
//  Copyright (c) 2013年 哈 哈 1. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DismissBlock)(int buttonIndex);
@interface UIAlertView(Extend)<UIAlertViewDelegate>

+(UIAlertView *)showAlertWithTitle:(NSString *) title
                         message:(NSString *)message 
                         cancelButtonTitle:(NSString *)cancelButtonTitle 
                         otherButtonTitle:(NSString *)otherButtonTitle
                         onDisinss:(DismissBlock) dismissed;

+ (void)quickAlertWithTitle:(NSString *)title messageTitle:(NSString *)messageTitle dismissTitle:(NSString *)dismissTitle;
@end
