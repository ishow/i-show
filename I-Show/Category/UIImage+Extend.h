//
//  UIImage+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extend)
+ (UIImage *)imageWithColor:(UIColor *)color;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;
@end
