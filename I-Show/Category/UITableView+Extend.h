//
//  UITableView+Extend.h
//  公用类
//
//  Created by 哈 哈 on 14-9-17.
//  Copyright (c) 2014年 mapabc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extend)

-(void)hidenCellLine;

-(void)hidenExtraCellLine;

- (void)reloadRow:(NSInteger)row inSection:(NSInteger)section;

- (void)reloadRowWithNoneAnimation:(NSInteger)row;
@end
