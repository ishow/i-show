//
//  UIAlertView+Extend.m
//  MyUtil
//
//  Created by 哈 哈 on 13-10-4.
//  Copyright (c) 2013年 哈 哈 1. All rights reserved.
//

#import "UIAlertView+Extend.h"

@implementation UIAlertView(Extend)
DismissBlock _dismissBlock;

+(UIAlertView *)showAlertWithTitle:(NSString *) title
                           message:(NSString *)message
                 cancelButtonTitle:(NSString *)cancelButtonTitle
                  otherButtonTitle:(NSString *)otherButtonTitle
                         onDisinss:(DismissBlock) dismissed{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitle, nil];
    [alert show];
    _dismissBlock = [dismissed copy];
    return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonInde{
    _dismissBlock(buttonInde);
}

+ (void)quickAlertWithTitle:(NSString *)title messageTitle:(NSString *)messageTitle dismissTitle:(NSString *)dismissTitle {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:messageTitle delegate:nil cancelButtonTitle:dismissTitle otherButtonTitles:nil];
    [alert show];
}
@end
